package geekbarains.material.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import geekbarains.material.App
import geekbarains.material.R
import geekbarains.material.ui.picture.PictureOfTheDayFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(App.instance.currentTheme)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, PictureOfTheDayFragment.newInstance())
                .commitNow()
        }
    }

    fun changeTheme(theme: Int) {
        Log.d("TEST", "change theme")
        App.instance.currentTheme = theme
        this.recreate()
    }
}

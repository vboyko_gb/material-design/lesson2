package geekbarains.material

import android.app.Application

class App : Application() {

    var currentTheme = R.style.AppTheme

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: App
    }
}